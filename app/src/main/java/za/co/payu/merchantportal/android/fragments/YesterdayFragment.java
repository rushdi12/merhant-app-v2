package za.co.payu.merchantportal.android.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import za.co.payu.merchantportal.android.R;
import za.co.payu.merchantportal.android.utils.AlertDialogManager;
import za.co.payu.merchantportal.android.utils.ConnectionDetector;
import za.co.payu.merchantportal.android.utils.NukeSSLCerts;
import za.co.payu.merchantportal.android.utils.SessionManager;


public class YesterdayFragment extends Fragment{



    // Connection detector class
    ConnectionDetector cd;
    AlertDialogManager adm;

    // flag for Internet connection status
    boolean isInternetPresent = false;

    // Session Manager Class
    SessionManager session;

    public YesterdayFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
/*


        /*//*******************************************************************************
        // MUST BE REMOVED WHEN TAKING THIS TO PRODUCTION
        /*//*******************************************************************************
        NukeSSLCerts.nuke();
        /*//*******************************************************************************
        // creating connection detector class instance
        cd = new ConnectionDetector(getActivity());

        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        adm = new AlertDialogManager();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
//            adm.showAlertDialog(getActivity(), "Internet Connection",
//                    "You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            adm.showAlertDialog(getActivity(), "No Internet Connection",
                    "You don't have internet connection.", false);
        }

        // Session Manager
        session = new SessionManager(getActivity());*/

        /**
         * Call this function whenever you want to check user login
         * This will redirect user to LoginActivity is he is not
         * logged in
         * */
//        session.checkLogin();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_yesterday, container, false);
    }

}
