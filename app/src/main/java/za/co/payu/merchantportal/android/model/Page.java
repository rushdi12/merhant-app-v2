package za.co.payu.merchantportal.android.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Rushdi.Hoosain on 2016/01/30.
 */


@JsonIgnoreProperties(ignoreUnknown = true)
public class Page {

    private Page(){

    }


    @JsonProperty("rel")
    private String size;

    @JsonProperty("total_elements")
    private String totalElements;


    @JsonProperty("total_pages")
    private String totalPages;


    @JsonProperty("number")
    private String number;


    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(String totalPages) {
        this.totalPages = totalPages;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(String totalElements) {
        this.totalElements = totalElements;
    }
}
