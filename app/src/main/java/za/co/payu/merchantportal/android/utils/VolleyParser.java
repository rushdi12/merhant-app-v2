package za.co.payu.merchantportal.android.utils;

import android.app.ProgressDialog;
import android.net.Uri;
import android.util.Log;
import android.util.Pair;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import za.co.payu.merchantportal.android.app.AppController;


/**
 * Created by Ramiz.Mohamed on 18/09/2015.
 */
public class VolleyParser {
    public static final String TAG = VolleyParser.class
            .getSimpleName();

    //	static String authorisation = "PayUv1-HMAC-SHA256 Store0174:signedHeaders=nonce;request_date,signature=156b554b2da9fb12c0f207fdef326b3d23c93e16233b74477958dfeb2e219d80";
//    static String authorisation = "PayUv1-HMAC-SHA256 weChat:signedHeaders=nonce;request_date,signature=156b554b2da9fb12c0f207fdef326b3d23c93e16233b74477958dfeb2e219d80";
//    static String automation = "true";
//    static String mockTransaction = "false";
//    // create method
//    static String requestDate = "02-10-2014 T 11:48:46.885 Z +0200";
//    //create method
//    static String nonce = "12345679";
//    //	static String customerIdType = "CONNECT_ID";
//    static String customerIdType = "WECHAT_ID";

    // These tags will be used to cancel the requests
    private String tag_json_obj = "jobj_req", tag_json_arry = "jarray_req";
    // This tag will be used to cancel the request
    private String tag_string_req = "string_req";

    JSONObject result = null;

    // constructor
    public VolleyParser() {
    }

//    Making json object request
    public JSONObject getJSONFromUrl(String url, final ProgressDialog pDialog, List<Pair> params){
//        http://stackoverflow.com/a/8314106/1635441 Pairs
//       http://ogrelab.ikratko.com/android-volley-with-get-and-post-parameters-example/


//        http://stackoverflow.com/a/27880096/1635441
        Uri.Builder builtUri = Uri.parse(url).buildUpon();
        // iterate via "New way to loop"
        //http://crunchify.com/how-to-iterate-through-java-list-4-way-to-iterate-through-loop/
//        System.out.println("\n==> Advance For Loop Example..");
        for (Pair temp : params) {
            if(!temp.first.toString().equals("")){
//                builtUri.buildUpon()
                builtUri.appendQueryParameter(temp.first.toString(), temp.second.toString());
//                .appendQueryParameter(FORMAT_PARAM, "json")
//                .appendQueryParameter(UNITS_PARAM, "metric")
//                .appendQueryParameter(DAYS_PARAM, Integer.toString(7))
                        //.build();
            }
//            builtUri.build();
        }
        builtUri.build();

        String finalUrl = builtUri.toString();
        Log.i("URLTest",finalUrl);


//        http://blog.codeint.com/sending-custom-headers-with-volley-android-networking-library/
//        http://stackoverflow.com/a/17050421/1635441
        CustomJSONObjectRequest jsonObjReq = new CustomJSONObjectRequest(Request.Method.GET,
                finalUrl, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        pDialog.hide();
                        result = response;
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "WHAT IS HAPPENING");
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            };
        });

//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
//                finalUrl, null,
//                new Response.Listener<JSONObject>() {
//
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.d(TAG, response.toString());
//                         pDialog.hide();
//                        result = response;
//                    }
//                }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d(TAG, "WHAT IS HAPPENING");
//                VolleyLog.d(TAG, "Error: " + error.getMessage());
//                // hide the progress dialog
//                pDialog.hide();
//            };
//        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq,
                tag_json_obj);

        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
//        return jsonObjReq;
        Log.i("Result", result.toString());
        return result;
    }

//    Making json array request
    public JsonArrayRequest getJSONArrayFromUrl(String url, final ProgressDialog pDialog){
        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        pDialog.hide();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                pDialog.hide();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req,
                tag_json_arry);

        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_arry);

        return req;
    }

    //    Making string request
    public StringRequest getStringFromUrl(String url, final ProgressDialog pDialog){
        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                pDialog.hide();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                pDialog.hide();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

        return strReq;
    }

    //    Making parameter post
    public JsonObjectRequest postParameterToUrl(String url, final ProgressDialog pDialog, final List<Pair> paramsList){
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        pDialog.hide();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                pDialog.hide();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                for (Pair temp : paramsList) {
                    params.put(temp.first.toString(), temp.second.toString());
                }
                return params;
            }

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                //sets a request header so the page receiving the request
                //will know what to do with it
//                httpPost.setHeader("Accept", "application/json");
//                httpPost.setHeader("Content-type", "application/json");
//
//                httpPost.addHeader(WalletConstants.httpHeaderAuthorisation, authorisation);
//                httpPost.addHeader(WalletConstants.httpHeaderAutomation, automation);
//                httpPost.addHeader(WalletConstants.httpHeaderMockTransaction, mockTransaction);
//                httpPost.addHeader(WalletConstants.httpHeaderRequestDate, requestDate);
//                httpPost.addHeader(WalletConstants.httpHeaderNonce, nonce);
//                httpPost.addHeader(WalletConstants.httpHeaderCustomerIdType, customerIdType);

//                if(url.contains(WalletConstants.handshakeURI) || url.contains(WalletConstants.paymentMethodsURI) || url.contains(WalletConstants.ordersURI) || url.contains(WalletConstants.paymentsURI)){
//                    String customerPinPassword = null;
//                    if(params.has("customer_pin")){
//                        try {
//                            customerPinPassword = params.getString(("customer_pin"));
//                        } catch (JSONException e) {
//                            // TODO Auto-generated catch block
//                            e.printStackTrace();
//                        }
//
//                        httpPost.addHeader(WalletConstants.httpHeaderPin, customerPinPassword);
//                    }else if(params.has("customer_password")){
//                        try {
//                            customerPinPassword = params.getString(("customer_password"));
//                        } catch (JSONException e) {
//                            // TODO Auto-generated catch block
//                            e.printStackTrace();
//                        }
//
//                        httpPost.addHeader(WalletConstants.httpHeaderPassword, customerPinPassword);
//                    }
//                }

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("apiKey", "xxxxxxxxxxxxxxx");
                return headers;
            }

        };

        return jsonObjReq;
    }
}
