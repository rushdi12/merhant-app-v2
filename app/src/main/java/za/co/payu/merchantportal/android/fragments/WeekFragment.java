package za.co.payu.merchantportal.android.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import za.co.payu.merchantportal.android.R;
import za.co.payu.merchantportal.android.adapter.DialogAdapter;
import za.co.payu.merchantportal.android.adapter.TransactionsAdapter;
import za.co.payu.merchantportal.android.app.AppController;
import za.co.payu.merchantportal.android.model.Content;
import za.co.payu.merchantportal.android.model.Link;
import za.co.payu.merchantportal.android.model.TransactionResponse;
import za.co.payu.merchantportal.android.model.TransactionSearchResponseDto;
import za.co.payu.merchantportal.android.utils.AlertDialogManager;
import za.co.payu.merchantportal.android.utils.ConnectionDetector;
import za.co.payu.merchantportal.android.utils.Constants;
import za.co.payu.merchantportal.android.utils.CustomJSONPortalAPiObjectRequest;
import za.co.payu.merchantportal.android.utils.NukeSSLCerts;
import za.co.payu.merchantportal.android.utils.SessionManager;


public class WeekFragment extends Fragment{

    public static final String TAG = WeekFragment.class
            .getSimpleName();

    private JSONObject transactionData;
    private RecyclerView recyclerView;
    private TransactionsAdapter mAdapter;
    // Progress Dialog
    private ProgressDialog pDialog;

    // Session Manager Class
    SessionManager session;

    // flag for Internet connection status
    boolean isInternetPresent = false;

    // Connection detector class
    ConnectionDetector cd;

    AlertDialogManager adm;

    List<TransactionSearchResponseDto> transactionListResult = new ArrayList<>();

    private DialogAdapter dialogAdapter;


    public WeekFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //*******************************************************************************
        // MUST BE REMOVED WHEN TAKING THIS TO PRODUCTION
        //*******************************************************************************
        NukeSSLCerts.nuke();
        //*******************************************************************************
        // creating connection detector class instance
        cd = new ConnectionDetector(getActivity());

        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        adm = new AlertDialogManager();

        // check for Internet status
        if (isInternetPresent) {
            // Internet Connection is Present
            // make HTTP requests
//            adm.showAlertDialog(getActivity(), "Internet Connection",
//                    "You have internet connection", true);
        } else {
            // Internet connection is not present
            // Ask user to connect to Internet
            adm.showAlertDialog(getActivity(), "No Internet Connection",
                    "You don't have internet connection.", false);
        }

        // Session Manager
        session = new SessionManager(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_week, container, false);

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

//        mAdapter = new ChartsAdapter(transactionListResult);
        mAdapter = new TransactionsAdapter(transactionListResult, getActivity());
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        //recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new WeekFragment.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                TransactionSearchResponseDto transaction = transactionListResult.get(position);
                //transaction
//                Movie movie = movieList.get(position);
                dialogAdapter = new DialogAdapter(getActivity(),transaction);

                DialogPlus dialog = DialogPlus.newDialog(getActivity())
                        .setAdapter(dialogAdapter)
                        .setOnItemClickListener(new OnItemClickListener() {
                            @Override
                            public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                            }
                        })
                        .setExpanded(true)  // This will enable the expand feature, (similar to android L share dialog)
                        .setMargin(50, 0, 50, 0)
                        .setGravity(Gravity.CENTER)
                        .create();
                dialog.show();
//                Toast.makeText(getActivity(), transaction.getPayuReference() + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));



        // Inflate the layout for this fragment
        return rootView;
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private WeekFragment.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final WeekFragment.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
    @Override
    public void onResume() {
        super.onResume();

        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Loading");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
//
//        // Load establishment details
//        // Building Parameters
//        List<Pair> pairs = new ArrayList<Pair>();
//        pairs.add(new Pair("id", "id"));
//        // Load establishment deals
//
//        VolleyParser volleyParser =  new VolleyParser();
//        transactionData = volleyParser.getJSONFromUrl(TransactionManager.URL_GET_TRANSACTIONS_FOR_TODAY_FOR_STORE,pDialog,pairs);
        retrieveTransactionData();
    }

    private void retrieveTransactionData(){

        Log.i("Building URL...", "Building url for transactions retrieval");

        pDialog.show();

//        username = Constants.auth_uaa_username;
//        password = Constants.auth_uaa_password;

        String url = Constants.environment_qa_api_address + Constants.environment_qa_portal_context + Constants.api_context + Constants.transactions_uri + Constants.for_previous_week_uri;
     //   String url = Constants.environment_local_portal_address + Constants.environment_local_portal_context + Constants.api_context + Constants.transactions_uri + Constants.for_previous_week_uri;

        // Building Parameters
        List<Pair> params = new ArrayList<Pair>();
        params.add(new Pair("batch_start", "0"));
        params.add(new Pair("batch_size", "20"));

//        http://stackoverflow.com/a/27880096/1635441
        Uri.Builder builtUri = Uri.parse(url).buildUpon();
        // iterate via "New way to loop"
        //http://crunchify.com/how-to-iterate-through-java-list-4-way-to-iterate-through-loop/
//        System.out.println("\n==> Advance For Loop Example..");
        for (Pair temp : params) {
            if(!temp.first.toString().equals("")){
                builtUri.appendQueryParameter(temp.first.toString(), temp.second.toString());
            }
        }
        builtUri.build();

        String finalUrl = builtUri.toString();
        Log.i("Transactions API url",finalUrl);

//        http://blog.codeint.com/sending-custom-headers-with-volley-android-networking-library/
//        http://stackoverflow.com/a/17050421/1635441
        CustomJSONPortalAPiObjectRequest jsonObjReq = new CustomJSONPortalAPiObjectRequest(Request.Method.GET,
                finalUrl, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        pDialog.hide();
                        prepareTransactionData(response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.getMessage());
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            };
        },session);

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq,
                "jobj_req");
    }

    /**
     * Called when the fragment is no longer in use.  This is called
     * after {@link #onStop()} and before {@link #onDetach()}.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        session.logoutUser();
    }

    private void prepareTransactionData(JSONObject response){

        TransactionResponse searchResponse = new TransactionResponse();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        List<Link> linkList = null;
        try {

            List<Content> contentList = new ArrayList<>();

            JSONArray contentArray = response.optJSONArray("content");
            for (int i = 0; i < contentArray.length(); i++) {
                //Content
                JSONObject contentObject = contentArray.getJSONObject(i);

                Content content = mapper.readValue(contentObject.toString(), Content.class);

                contentList.add(content);
            }
            searchResponse.setContent(contentList);
            processResponse(searchResponse);

        } catch (Exception e) {
            Log.e("Error parsing Json", e.getMessage());
        }
        Log.i("searchResponse" , "content" + searchResponse.getContent());

//        mAdapter.notifyDataSetChanged();
    }

    private void processResponse(TransactionResponse searchResponse){
        List<Content> contentList = searchResponse.getContent();

        transactionListResult.clear();

        for(Content content : contentList) {

            TransactionSearchResponseDto searchResponseDto = new TransactionSearchResponseDto();
            searchResponseDto.setBasketAmount(content.getBasketAmount());
            searchResponseDto.setPayuReference(content.getPayUReference());
            searchResponseDto.setTransactionType(content.getTransactionType());
            searchResponseDto.setMerchantReference(content.getMerchantReference());
            searchResponseDto.setTransactionDate(content.getReceivedTime());
            searchResponseDto.setTranactionStatus(content.getTransactionStatus());
            searchResponseDto.setPaymentMethodsUsed(content.getPaymentMethods());
            searchResponseDto.setStoreName(content.getStoreName());
            searchResponseDto.setStoreId(content.getStoreId());

            transactionListResult.add(searchResponseDto);
        }

        // transactionListAdapter.addItems(transactionListResult);

        // transactionListAdapter.addAll(transactionListResult);


       // mAdapter = new TransactionsAdapter(transactionListResult, getActivity());

        mAdapter.notifyDataSetChanged();
    }
}
