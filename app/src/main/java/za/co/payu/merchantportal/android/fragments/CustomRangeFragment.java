package za.co.payu.merchantportal.android.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anton46.collectionitempicker.CollectionPicker;
import com.anton46.collectionitempicker.Item;
import com.anton46.collectionitempicker.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

import za.co.payu.merchantportal.android.R;
import za.co.payu.merchantportal.android.utils.AlertDialogManager;
import za.co.payu.merchantportal.android.utils.ConnectionDetector;
import za.co.payu.merchantportal.android.utils.NukeSSLCerts;
import za.co.payu.merchantportal.android.utils.SessionManager;


public class CustomRangeFragment extends Fragment{

    // Progress Dialog
    private ProgressDialog pDialog;

    // Session Manager Class
    SessionManager session;

    // flag for Internet connection status
    boolean isInternetPresent = false;

    // Connection detector class
    ConnectionDetector cd;

    AlertDialogManager adm;

    CollectionPicker mPicker;
    TextView mTextView;

    int counter;

    public CustomRangeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        super.onCreate(savedInstanceState);

        //*******************************************************************************
        // MUST BE REMOVED WHEN TAKING THIS TO PRODUCTION
        //*******************************************************************************
        NukeSSLCerts.nuke();
        //*******************************************************************************
        // creating connection detector class instance
        cd = new ConnectionDetector(getActivity());

        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        adm = new AlertDialogManager();

        // check for Internet status
        if (!isInternetPresent) {
            adm.showAlertDialog(getActivity(), "No Internet Connection",
                    "You don't have internet connection.", false);
        }
        // Session Manager
        session = new SessionManager(getActivity());



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_custom_range, container, false);


//Warrens gay
        mPicker = (CollectionPicker) rootView.findViewById(R.id.collection_item_picker);
        mPicker.setItems(generateItems());
        mPicker.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onClick(Item item, int position) {
                if (item.isSelected) {
                    counter++;
                } else {
                    counter--;
                }
                mTextView.setText(counter + " Items Selected");
            }
        });

        mTextView = (TextView) rootView.findViewById(R.id.text);

        // Inflate the layout for this fragment
        return rootView;
    }


    private List<Item> generateItems() {
        List<Item> items = new ArrayList<>();
        items.add(new Item("a", "Successful"));
        items.add(new Item("b", "Failed"));
        items.add(new Item("c", "Processing"));
        items.add(new Item("d", "Payment"));
        items.add(new Item("e", "Reserve"));
        items.add(new Item("f", "Finalize"));
        return items;
    }

}
