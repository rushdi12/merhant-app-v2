package za.co.payu.merchantportal.android.model;

import java.util.List;

/**
 * Created by Rushdi.Hoosain on 2016/01/31.
 */
public class TransactionSearchResponseDto {

    private String transactionType;
    private String payuReference;
    private String merchantReference;
    private String basketAmount;
    private String tranactionStatus;
    private String transactionDate;
    private List<PaymentMethod> paymentMethodsUsed;
    private String storeName;
    private String storeId;

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getBasketAmount() {
        return basketAmount;
    }

    public void setBasketAmount(String basketAmount) {
        this.basketAmount = basketAmount;
    }

    public String getPayuReference() {
        return payuReference;
    }

    public void setPayuReference(String payuReference) {
        this.payuReference = payuReference;
    }

    public String getMerchantReference() {
        return merchantReference;
    }

    public void setMerchantReference(String merchantReference) {
        this.merchantReference = merchantReference;
    }

    public String getTranactionStatus() {
        return tranactionStatus;
    }

    public void setTranactionStatus(String tranactionStatus) {
        this.tranactionStatus = tranactionStatus;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public List<PaymentMethod> getPaymentMethodsUsed() {
        return paymentMethodsUsed;
    }

    public void setPaymentMethodsUsed(List<PaymentMethod> paymentMethodsUsed) {
        this.paymentMethodsUsed = paymentMethodsUsed;
    }
}
