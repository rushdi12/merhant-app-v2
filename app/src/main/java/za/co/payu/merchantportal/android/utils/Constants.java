package za.co.payu.merchantportal.android.utils;

public class Constants {

	public static final String REKO_API_KEY = "6MIzSN4a8gAHcT23";
	public static final String REKO_API_SECRET = "73oBwvC4uO40JKys";
	public static final String NAMESPACE = "face_it";
	public static final String TAG = "payuwallet";

	//localhost UAA
	public static final String auth_uaa_client_id = "portal_ui";
	public static final String auth_client_secret = "Payu12345";
	public static final String auth_uaa_grant_type = "password";
	public static final String auth_uaa_password = "Payu12345";
	public static final String auth_uaa_redirect_uri = "http://example.com";
	public static final String auth_uaa_username = "ops@example.com";
	//	public static final String auth_uaa_username = "ops@example.com";
	public static final String environment_local_uaa_address = "http://10.0.2.2:8082";

	public static final String environment_local_uaa_context = "/uaa";
	public static final String uri_auth_token = "/oauth/token";

	//Localhost Portal api
	public static final String environment_local_portal_address = "http://10.0.2.2:8070";
	public static final String environment_local_portal_context = "/portal";

	//QA UAA
	/*public static final String environment_qa_uaa_address = "https://196.28.152.190:8443";
	public static final String environment_qa_uaa_context = "/uaa";
*/

	//THIS QA-RELEASE WORKS
/*
	public static final String environment_qa_uaa_address = "https://qa-release.payu.co.za:8087";
	public static final String environment_qa_uaa_context = "/uaa";
*/

	public static final String environment_qa_uaa_address = "https://stagingmerchantportal.payu.co.za:443";
	public static final String environment_qa_uaa_context = "/uaa";


	//QAPortal api
	/*public static final String environment_qa_portal_address = "https://qa-release.payu.co.za:8086";
	public static final String environment_qa_portal_context = "/portal";
*/

	public static final String environment_qa_portal_address = "https://stagingmerchantportal.payu.co.za";
	public static final String environment_qa_portal_context = "/portal";


	// transactions api

	/*public static final String environment_qa_api_address = "https://qa-release.payu.co.za:8086";
	public static final String api_context = "/api/v1/";
*/

	public static final String environment_qa_api_address = "https://stagingmerchantportal.payu.co.za";
	public static final String api_context = "/api/v1/";


	//	public static final String api_base_url = "/api/v1/";
	public static final String transactions_uri = "transactions";
	public static final String by_payu_reference_uri= "/by-payu-ref/";
	public static final String by_merchant_reference_uri= "/by-merchant-ref/";

	public static final String stores_context = "stores/";

	public static final String for_today_uri = "/for-today";

	public static final String for_previous_week_uri = "/for-previous-week";



	//api timout
	public static final String setting_timeout = "20000";



	//Batman api
	public static final String environment_batman_address = "http://beta.warrenroman.com";
	public static final String environment_batman_context = "/payu";
	public static final String environment_batman_path = "/wallet-qr-code";
	public static final String environment_batman_uri = "/getQrCodeUrl";
}
