package za.co.payu.merchantportal.android.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

/**
 * Created by Rushdi.Hoosain on 2016/01/31.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentMethod {


    @JsonProperty("payment_method_type")
    private String paymentMethodType;

    @JsonProperty("payment_instrument_type")
    private String paymentInstrumentType;

    @JsonProperty("tender_type")
    private String tenderType;

    //	@JsonUnwrapped
    @JsonProperty("values")
    private Map<String,String> values;

    @JsonProperty("secure_3D_Info")
    private Secure3DInfo secure3DInfo;


    public String getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    public String getPaymentInstrumentType() {
        return paymentInstrumentType;
    }

    public void setPaymentInstrumentType(String paymentInstrumentType) {
        this.paymentInstrumentType = paymentInstrumentType;
    }

    public Map<String, String> getValues() {
        return values;
    }

    public void setValues(Map<String, String> values) {
        this.values = values;
    }

    public Secure3DInfo getSecure3DInfo() {
        return secure3DInfo;
    }

    public void setSecure3DInfo(Secure3DInfo secure3DInfo) {
        this.secure3DInfo = secure3DInfo;
    }

    public String getTenderType() {
        return tenderType;
    }

    public void setTenderType(String tenderType) {
        this.tenderType = tenderType;
    }
}


