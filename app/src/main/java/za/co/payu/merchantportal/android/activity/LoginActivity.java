package za.co.payu.merchantportal.android.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import za.co.payu.merchantportal.android.R;
import za.co.payu.merchantportal.android.app.AppController;
import za.co.payu.merchantportal.android.utils.AlertDialogManager;
import za.co.payu.merchantportal.android.utils.ConnectionDetector;
import za.co.payu.merchantportal.android.utils.Constants;
import za.co.payu.merchantportal.android.utils.CustomJSONObjectRequest;
import za.co.payu.merchantportal.android.utils.NukeSSLCerts;
import za.co.payu.merchantportal.android.utils.SessionManager;
import za.co.payu.merchantportal.android.utils.UAAFunctions;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {
    public static final String TAG = LoginActivity.class
            .getSimpleName();

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;


    public static final String REQUEST_TAG = "LoginActivity";
    private RequestQueue mQueue;

    private EditText editTextReponse;
    private String username = null;
    private String password = null;

    ProgressDialog progressDialog;

    // Session Manager Class
    SessionManager session;

    // flag for Internet connection status
    boolean isInternetPresent = false;

    // Connection detector class
    ConnectionDetector cd;

    AlertDialogManager adm;

    // temporary string to show the parsed response
    private JSONObject jsonResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // creating connection detector class instance
        cd = new ConnectionDetector(getApplicationContext());

        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        adm = new AlertDialogManager();

        // check for Internet status
        if (!isInternetPresent) {
            adm.showAlertDialog(LoginActivity.this, "No Internet Connection",
                    "You don't have internet connection.", false);

        }

        // Session Manager
        session = new SessionManager(getApplicationContext());

        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");

        //*******************************************************************************
        // MUST BE REMOVED WHEN TAKING THIS TO PRODUCTION
        //*******************************************************************************
        NukeSSLCerts.nuke();
        //*******************************************************************************

        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    login();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);


    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void login() {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.

        username = mEmailView.getText().toString();
        password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(username)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(username)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }


        super.onStart();
        session.logoutUser();

        if (!session.isLoggedIn()) {
            getToken();
        }
        else {
            Log.i(TAG, "Token exists");
            // get user data from session
            HashMap<String, String> user = session.getUserDetails();

            // name
            String userToken = user.get(SessionManager.KEY_TOKEN);
            Intent i = new Intent(getApplicationContext(), ScrollableTabsActivity.class);
            startActivity(i);
            finish();
        }

    }

   @Override
    protected void onStop() {
        super.onStop();

        if(mQueue != null) {
            mQueue.cancelAll(REQUEST_TAG);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }

    private void getToken(){
        Log.i("Building URL...", "Building url for token retrieval");

        progressDialog.show();

        username = Constants.auth_uaa_username;
        password = Constants.auth_uaa_password;

        String url = Constants.environment_qa_uaa_address + Constants.environment_qa_uaa_context + Constants.uri_auth_token;

        // Building Parameters
        List<Pair> params = new ArrayList<Pair>();
        params.add(new Pair("grant_type", Constants.auth_uaa_grant_type));
        params.add(new Pair("client_id", Constants.auth_uaa_client_id));
        params.add(new Pair("redirect_uri", Constants.auth_uaa_redirect_uri));
        params.add(new Pair("username", username));
        params.add(new Pair("password", password));

//        http://stackoverflow.com/a/27880096/1635441
        Uri.Builder builtUri = Uri.parse(url).buildUpon();

        for (Pair temp : params) {
            if(!temp.first.toString().equals("")){
                builtUri.appendQueryParameter(temp.first.toString(), temp.second.toString());
            }
        }
        builtUri.build();

        String finalUrl = builtUri.toString();
        Log.i("URLTest",finalUrl);

        CustomJSONObjectRequest jsonObjReq = new CustomJSONObjectRequest(Request.Method.GET,
                finalUrl, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        progressDialog.dismiss();
                        jsonResponse = response;
                        try{
                            // Creating session
                            session.createLoginSession(jsonResponse.getString("access_token"), jsonResponse.getString("token_type"));
                            Intent i = new Intent(getApplicationContext(), ScrollableTabsActivity.class);
                            startActivity(i);
                            finish();
                        }catch (JSONException e){
                            Log.e("JSON parsing", e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "WHAT IS HAPPENING");
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                progressDialog.dismiss();
            };
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq,
                "jobj_req");
    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }
}

