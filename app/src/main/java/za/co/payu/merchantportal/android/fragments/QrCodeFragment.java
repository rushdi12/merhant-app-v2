package za.co.payu.merchantportal.android.fragments;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import net.soulwolf.widget.materialradio.MaterialRadioButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import za.co.payu.merchantportal.android.R;
import za.co.payu.merchantportal.android.adapter.DialogAdapter;
import za.co.payu.merchantportal.android.adapter.QrCodeAdapter;
import za.co.payu.merchantportal.android.adapter.TransactionsAdapter;
import za.co.payu.merchantportal.android.app.AppController;
import za.co.payu.merchantportal.android.model.Content;
import za.co.payu.merchantportal.android.model.Link;
import za.co.payu.merchantportal.android.model.TransactionResponse;
import za.co.payu.merchantportal.android.model.TransactionSearchResponseDto;
import za.co.payu.merchantportal.android.utils.AlertDialogManager;
import za.co.payu.merchantportal.android.utils.ConnectionDetector;
import za.co.payu.merchantportal.android.utils.Constants;
import za.co.payu.merchantportal.android.utils.CustomJSONPortalAPiObjectRequest;
import za.co.payu.merchantportal.android.utils.LruBitmapCache;
import za.co.payu.merchantportal.android.utils.NukeSSLCerts;
import za.co.payu.merchantportal.android.utils.SessionManager;


public class QrCodeFragment extends Fragment {

    public static final String TAG = QrCodeFragment.class
            .getSimpleName();

    private JSONObject transactionData;
    private RecyclerView recyclerView;
    private QrCodeAdapter mAdapter;
    // Progress Dialog
    private ProgressDialog pDialog;
    // Session Manager Class
    private SessionManager session;
    // flag for Internet connection status
    boolean isInternetPresent = false;
    // Connection detector class
    private  ConnectionDetector cd;
    private AlertDialogManager adm;

    private QrCodeAdapter dialogAdapter;

    private NetworkImageView mNetworkImageView;

    private ImageLoader mImageLoader;

   private Button genButton;
    String qrcodeUrl;
    MaterialRadioButton payuWalletRadioButton;
    MaterialRadioButton snapScanRadioButton;

    public QrCodeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //*******************************************************************************
        // MUST BE REMOVED WHEN TAKING THIS TO PRODUCTION
        //*******************************************************************************
        NukeSSLCerts.nuke();
        //*******************************************************************************
        // creating connection detector class instance
        cd = new ConnectionDetector(getActivity());

        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        adm = new AlertDialogManager();

        // check for Internet status
        if (!isInternetPresent) {
            adm.showAlertDialog(getActivity(), "No Internet Connection",
                    "You don't have internet connection.", false);
        }
        // Session Manager
        session = new SessionManager(getActivity());

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       final View rootView = inflater.inflate(R.layout.fragment_qr_code_layout, container, false);

       payuWalletRadioButton = (MaterialRadioButton) rootView.findViewById(R.id.payuwallet);
       snapScanRadioButton = (MaterialRadioButton) rootView.findViewById(R.id.snapscan);

       genButton = (Button) rootView.findViewById(R.id.generateQrCodeBtn);

       genButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 retrieveQrCode(rootView);
            }
        });


       return rootView;

    }



/*    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private QrCodeFragment.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final QrCodeFragment.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }*/

    /**
     * Called when the fragment is visible to the user and actively running.
     * This is generally
     * tied to {@link Activity#onResume() Activity.onResume} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onResume() {
        super.onResume();
    }

    private void retrieveQrCode(View v){



        Log.i("Building URL...", "Building url for qrcode retrieval");

        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Loading");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();

        mNetworkImageView = (NetworkImageView) v.findViewById(R.id.qrcodeImageView);


        EditText amountEditText = (EditText) v.findViewById(R.id.amount);
        EditText descriptionEditText = (EditText) v.findViewById(R.id.description);
        EditText emailEditText = (EditText) v.findViewById(R.id.email);

        String amount = amountEditText.getText().toString();
        String description = descriptionEditText.getText().toString();
        String email = emailEditText.getText().toString();

        String storeId = "201123";

        String url = Constants.environment_batman_address + Constants.environment_batman_context + Constants.environment_batman_path + "/" + storeId + Constants.environment_batman_uri;

        // Building Parameters
        List<Pair> params = new ArrayList<Pair>();
        params.add(new Pair("amountInCents", amount));
        params.add(new Pair("description", description));
        params.add(new Pair("customerNotification", email));
        params.add(new Pair("merchantNotification", "warren.roman@payu.co.za" + ",0720123456"));


//        http://stackoverflow.com/a/27880096/1635441
        Uri.Builder builtUri = Uri.parse(url).buildUpon();
        // iterate via "New way to loop"
        //http://crunchify.com/how-to-iterate-through-java-list-4-way-to-iterate-through-loop/
//        System.out.println("\n==> Advance For Loop Example..");
        for (Pair temp : params) {
            if(!temp.first.toString().equals("")){
                builtUri.appendQueryParameter(temp.first.toString(), temp.second.toString());
            }
        }
        builtUri.build();

        String finalUrl = builtUri.toString();
        Log.i("Qr code API url",finalUrl);

//        http://blog.codeint.com/sending-custom-headers-with-volley-android-networking-library/
//        http://stackoverflow.com/a/17050421/1635441
        CustomJSONPortalAPiObjectRequest jsonObjReq = new CustomJSONPortalAPiObjectRequest(Request.Method.GET,
                finalUrl, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        pDialog.hide();
                        handleResponse(response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                String errorMessage = error.getMessage() != null ? error.getMessage() : error.toString();
                Log.d(TAG, errorMessage);
                VolleyLog.d(TAG, "Error: " + errorMessage);
                // hide the progress dialog
                pDialog.hide();
            };
        },session);

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq,
                "jobj_req");
    }

    /**
     * Called when the fragment is no longer in use.  This is called
     * after {@link #onStop()} and before {@link #onDetach()}.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void handleResponse(JSONObject response){

        Log.i("Batman response", response.toString());

        // unmarshall the response
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            JSONObject paymnetMethodObject = response.getJSONObject("paymentMethod");

            JSONObject snapScanObject =  paymnetMethodObject.getJSONObject("snapscan");
            String snapScanUrl = snapScanObject.getString("qrCodeUrl");
            JSONObject payuWalletObject =  paymnetMethodObject.getJSONObject("payuWallet");
            String payuWalletUrl = payuWalletObject.getString("qrCodeUrl");

            if(payuWalletRadioButton.isChecked()){
                qrcodeUrl = payuWalletUrl;

            } else if (snapScanRadioButton.isChecked()) {
                qrcodeUrl = snapScanUrl;
            }

            showQrCodeDialog();

        } catch (Exception e) {
            Log.e("Error parsing Json", e.getLocalizedMessage());
        }
    }


    private void showQrCodeDialog(){

        dialogAdapter = new QrCodeAdapter(getActivity(), qrcodeUrl);

            DialogPlus dialog = DialogPlus.newDialog(getActivity())
                .setAdapter(dialogAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .setGravity(Gravity.CENTER)
                .setCancelable(true)
                    .setContentHeight(ViewGroup.LayoutParams.MATCH_PARENT)
                    .setContentWidth(ViewGroup.LayoutParams.MATCH_PARENT)
                .create();
        dialog.show();
    }
}
