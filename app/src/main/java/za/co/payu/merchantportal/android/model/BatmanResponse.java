package za.co.payu.merchantportal.android.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Rushdi.Hoosain on 2016/01/30.
 */


@JsonIgnoreProperties(ignoreUnknown = true)
public class BatmanResponse {

    @JsonProperty("paymentMethod")
    private List<String> paymentMethod;

    public List<String> getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(List<String> paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
}
