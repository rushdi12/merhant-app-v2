package za.co.payu.merchantportal.android.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Rushdi.Hoosain on 2016/01/31.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Secure3DInfo {

    @JsonProperty("lookup_Error_Description")
    private String lookupErrorDescription;

    @JsonProperty("lookup_Error_No")
    private String lookupErrorNo;

    @JsonProperty("lookup_Enrolled")
    private String lookupEnrolled;

    @JsonProperty("lookup_Eci_Flag")
    private String lookupEciFlag;

    @JsonProperty("lookup_Start_Date_Time")
    private String lookupStartDateTime;

    @JsonProperty("lookup_End_Date_Time")
    private String lookupEndDateTime;

    @JsonProperty("authenticate_Send")
    private String authenticateSend;

    @JsonProperty("authenticate_Error_No")
    private String authenticateErrorNo;

    @JsonProperty("authenticate_Error_Description")
    private String authenticateErrorDescription;

    @JsonProperty("authenticate_Cavv")
    private String authenticateCavv;

    @JsonProperty("authenticate_Xid")
    private String authenticateXid;

    @JsonProperty("authenticate_Eci_Flag")
    private String authenticateEciFlag;

    @JsonProperty("authenticate_PARes_Status")
    private String authenticatePAResStatus;

    @JsonProperty("authenticate_Start_Date_Time")
    private String authenticateStartDateTime;

    @JsonProperty("authenticate_End_Date_Time")
    private String authenticateEndDateTime;;

    public String getLookupErrorDescription() {
        return lookupErrorDescription;
    }

    public void setLookupErrorDescription(String lookupErrorDescription) {
        this.lookupErrorDescription = lookupErrorDescription;
    }

    public String getLookupErrorNo() {
        return lookupErrorNo;
    }

    public void setLookupErrorNo(String lookupErrorNo) {
        this.lookupErrorNo = lookupErrorNo;
    }

    public String getLookupEnrolled() {
        return lookupEnrolled;
    }

    public void setLookupEnrolled(String lookupEnrolled) {
        this.lookupEnrolled = lookupEnrolled;
    }

    public String getLookupStartDateTime() {
        return lookupStartDateTime;
    }

    public void setLookupStartDateTime(String lookupStartDateTime) {
        this.lookupStartDateTime = lookupStartDateTime;
    }

    public String getLookupEciFlag() {
        return lookupEciFlag;
    }

    public void setLookupEciFlag(String lookupEciFlag) {
        this.lookupEciFlag = lookupEciFlag;
    }

    public String getAuthenticateErrorNo() {
        return authenticateErrorNo;
    }

    public void setAuthenticateErrorNo(String authenticateErrorNo) {
        this.authenticateErrorNo = authenticateErrorNo;
    }

    public String getAuthenticateSend() {
        return authenticateSend;
    }

    public void setAuthenticateSend(String authenticateSend) {
        this.authenticateSend = authenticateSend;
    }

    public String getLookupEndDateTime() {
        return lookupEndDateTime;
    }

    public void setLookupEndDateTime(String lookupEndDateTime) {
        this.lookupEndDateTime = lookupEndDateTime;
    }

    public String getAuthenticateStartDateTime() {
        return authenticateStartDateTime;
    }

    public void setAuthenticateStartDateTime(String authenticateStartDateTime) {
        this.authenticateStartDateTime = authenticateStartDateTime;
    }

    public String getAuthenticateEndDateTime() {
        return authenticateEndDateTime;
    }

    public void setAuthenticateEndDateTime(String authenticateEndDateTime) {
        this.authenticateEndDateTime = authenticateEndDateTime;
    }

    public String getAuthenticateEciFlag() {
        return authenticateEciFlag;
    }

    public void setAuthenticateEciFlag(String authenticateEciFlag) {
        this.authenticateEciFlag = authenticateEciFlag;
    }

    public String getAuthenticateXid() {
        return authenticateXid;
    }

    public void setAuthenticateXid(String authenticateXid) {
        this.authenticateXid = authenticateXid;
    }

    public String getAuthenticateErrorDescription() {
        return authenticateErrorDescription;
    }

    public void setAuthenticateErrorDescription(String authenticateErrorDescription) {
        this.authenticateErrorDescription = authenticateErrorDescription;
    }

    public String getAuthenticateCavv() {
        return authenticateCavv;
    }

    public void setAuthenticateCavv(String authenticateCavv) {
        this.authenticateCavv = authenticateCavv;
    }

    public String getAuthenticatePAResStatus() {
        return authenticatePAResStatus;
    }

    public void setAuthenticatePAResStatus(String authenticatePAResStatus) {
        this.authenticatePAResStatus = authenticatePAResStatus;
    }
}


