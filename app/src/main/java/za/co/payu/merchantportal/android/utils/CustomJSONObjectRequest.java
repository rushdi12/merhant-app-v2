package za.co.payu.merchantportal.android.utils;

import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Rushdi.Hoosain on 2016/01/24.
 */
public class CustomJSONObjectRequest extends JsonObjectRequest {

    public CustomJSONObjectRequest(int method, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        String credentials = "portal_ui:Payu12345";

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Basic " + new String(Base64.encode(credentials.getBytes(), Base64.NO_WRAP)));

        Log.i("Authorization ", new String(Base64.encode(credentials.getBytes(), Base64.NO_WRAP)));

        return headers;
    }

    @Override
    public RetryPolicy getRetryPolicy() {
        // we can write custom retry policy
        return super.getRetryPolicy();
    }

    @Override
    public String getBodyContentType() {
        return "application/json";
    }
}
