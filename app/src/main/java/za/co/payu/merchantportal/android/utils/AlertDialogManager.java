package za.co.payu.merchantportal.android.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
 
public class AlertDialogManager {
    /**
     * Function to display simple Alert Dialog
     * @param context - application context
     * @param title - alert dialog title
     * @param message - alert message
     * @param status - success/failure (used to set icon)
     *               - pass null if you don't want icon
     * */
    public void showAlertDialog(Context context, String title, String message,
            Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        //Log.i("Activity", context.getClass().getSimpleName());
        
        final Activity activity = (Activity) context;
        // Setting Dialog Title
        alertDialog.setTitle(title);
 
        // Setting Dialog Message
        alertDialog.setMessage(message);
        
        //alertDialog.setIcon(R.drawable.app_icon_);
 
        if(status != null)
            // Setting alert dialog icon
            //alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
 
 //http://stackoverflow.com/a/8228190/1635441
        	//if(context.getClass().getSimpleName().equals("PaymentFormActivity")){
//        	if(activity.getClass().getSimpleName().equals("PaymentFormActivity") || activity.getClass().getSimpleName().equals("CashTopUpActivity")){
//			  alertDialog.setButton( Dialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
//			      public void onClick(DialogInterface dialog, int which) {
//			    	  activity.finish();
//			    	  //context.get
//			      }
//
//			  });
//
////			  alertDialog.setButton( Dialog.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener()    {
////			     public void onClick(DialogInterface dialog, int which) {
////			    	 activity.startActivity(new Intent(activity, WalletActivity.class));
////			    	 activity.finish();
////			     }
////
////			  });
//        	}else{
		        // Setting OK Button
		        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int which) {
		            }
		        });
//        	}
        
        //http://stackoverflow.com/a/13063444/1635441
        //TextView messageText = (TextView)alertDialog.findViewById(android.R.id.message);
        //messageText.setGravity(Gravity.CENTER);
 
        // Showing Alert Message
        alertDialog.show();
    }
}
