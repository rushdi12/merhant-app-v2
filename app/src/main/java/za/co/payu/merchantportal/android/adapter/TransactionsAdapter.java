package za.co.payu.merchantportal.android.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kd.dynamic.calendar.generator.ImageGenerator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import za.co.payu.merchantportal.android.R;
import za.co.payu.merchantportal.android.model.PaymentMethod;
import za.co.payu.merchantportal.android.model.TransactionSearchResponseDto;

public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.MyViewHolder> {

    private List<TransactionSearchResponseDto> transactionsList;
    private Activity activity;

    CardView cardView;

    View statusIndicator;

    ImageView mDisplayGeneratedImage,pm;

    ImageGenerator mImageGenerator;

    Calendar mTransactionDate;

    Bitmap mGeneratedDateIcon;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView merchantReference, basketAmount, payUReference, transactionType;

        public MyViewHolder(View view) {
            super(view);
            cardView = (CardView)view.findViewById(R.id.cardview);
            statusIndicator = (View) view.findViewById(R.id.status_indicator);
            merchantReference = (TextView) view.findViewById(R.id.merchantReference);
            payUReference = (TextView) view.findViewById(R.id.payUReference);
            basketAmount = (TextView) view.findViewById(R.id.basketAmount);
            transactionType = (TextView) view.findViewById(R.id.transactionType);
            pm = (ImageView) view.findViewById(R.id.pm);

            mDisplayGeneratedImage = (ImageView) view.findViewById(R.id.imgGenerated);

            mImageGenerator = new ImageGenerator(activity);

            mImageGenerator.setIconSize(50, 50);
            mImageGenerator.setDateSize(30);
            mImageGenerator.setMonthSize(15);

            mImageGenerator.setDatePosition(42);
            mImageGenerator.setMonthPosition(14);

            mImageGenerator.setDateColor(R.color.payuGreen);
            mImageGenerator.setMonthColor(Color.WHITE);


        }
    }


    public TransactionsAdapter(List<TransactionSearchResponseDto> transactionsList, Activity activity){
        this.transactionsList = transactionsList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_transaction, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Date date = null;
        TransactionSearchResponseDto transaction = transactionsList.get(position);
        holder.merchantReference.setText(transaction.getMerchantReference());
        holder.payUReference.setText(transaction.getPayuReference());
        holder.basketAmount.setText(transaction.getBasketAmount());

        String paymentMethod = "" ;
//        String list = "";
        for(PaymentMethod pms :transaction.getPaymentMethodsUsed()){
            paymentMethod += pms.getPaymentMethodType();
            if (paymentMethod.equals("CREDITCARD")){
                pm.setBackgroundResource(R.drawable.pm_creditcard);
            }else if(paymentMethod.equals("EFT")){
                pm.setBackgroundResource(R.drawable.pm_eft);
            }else{
                pm.setBackgroundResource(R.drawable.pm_loyalty);
            }
        }

        holder.transactionType.setText(transaction.getTransactionType());

        String transactionStatus = transaction.getTranactionStatus();

        if (transactionStatus.equals("SUCCESSFUL")){
            statusIndicator.setBackgroundColor(activity.getResources().getColor(R.color.payuGreen));
        }else if(transactionStatus.equals("FAILED")){
            statusIndicator.setBackgroundColor(Color.RED);
        }

        String transactionDateTime = transaction.getTransactionDate();

        try {
            date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(transactionDateTime);
        }catch (ParseException e){
            Log.e("Date parse error", e.toString());
        }

//        int transactionYear = 0, transactionMonth = 0, transactionDay = 0;

        // Set the mCurrentDate to the selected date-month-year
        mTransactionDate = Calendar.getInstance();
        mTransactionDate.set(date.getYear(), date.getMonth(), date.getDay());
        mGeneratedDateIcon = mImageGenerator.generateDateImage(mTransactionDate, R.drawable.empty_calendar_payu);
        mDisplayGeneratedImage.setImageBitmap(mGeneratedDateIcon);
    }

    @Override
    public int getItemCount() {
        return transactionsList.size();
    }


}
