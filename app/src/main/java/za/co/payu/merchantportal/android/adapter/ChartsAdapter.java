package za.co.payu.merchantportal.android.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;

import org.json.JSONObject;

import za.co.payu.merchantportal.android.R;

//http://stackoverflow.com/a/32267521/1635441
public class ChartsAdapter extends RecyclerView.Adapter<ChartsAdapter.MyViewHolder> {

    private JSONObject transactionData;
    Context context;

    public ChartsAdapter(JSONObject transactionData) {
        this.transactionData = transactionData;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
//        CardView cardView;
//        private PieChart mPieChart;
//        private BarChart mBarChart;
//        private LineChart mLineChart;

        public MyViewHolder(View view) {
            super(view);
//            cardView = (CardView)view.findViewById(R.id.cardview);
//            mPieChart = (PieChart) view.findViewById(R.id.pie_chart);
//            mBarChart = (BarChart) view.findViewById(R.id.bar_chart);
//            mLineChart = (LineChart) view.findViewById(R.id.line_chart);
        }
    }

    public class PieChartViewHolder extends MyViewHolder {
        CardView cardView;
        private PieChart mPieChart;

        public PieChartViewHolder(View view) {
            super(view);
            cardView = (CardView)view.findViewById(R.id.cardview);
            mPieChart = (PieChart) view.findViewById(R.id.pie_chart);

            mPieChart.setDescription("");

            Typeface tf = Typeface.createFromAsset(context.getAssets(), "OpenSans-Light.ttf");

            mPieChart.setCenterTextTypeface(tf);
            mPieChart.setCenterText(generateCenterText());
            mPieChart.setCenterTextSize(10f);
            mPieChart.setCenterTextTypeface(tf);

            // radius of the center hole in percent of maximum radius
            mPieChart.setHoleRadius(45f);
            mPieChart.setTransparentCircleRadius(50f);

            Legend l = mPieChart.getLegend();
            l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);

//            mPieChart.setData(generatePieData());
        }
    }

    private SpannableString generateCenterText() {
        SpannableString s = new SpannableString("Revenues\nQuarters 2015");
        s.setSpan(new RelativeSizeSpan(2f), 0, 8, 0);
        s.setSpan(new ForegroundColorSpan(Color.GRAY), 8, s.length(), 0);
        return s;
    }

    public class BarChartViewHolder extends MyViewHolder {
        CardView cardView;
        private BarChart mBarChart;

        public BarChartViewHolder(View view) {
            super(view);
            cardView = (CardView)view.findViewById(R.id.cardview);
            mBarChart = (BarChart) view.findViewById(R.id.bar_chart);
        }
    }

    public class LineChartViewHolder extends MyViewHolder {
        CardView cardView;
        private LineChart mLineChart;

        public LineChartViewHolder(View view) {
            super(view);
            cardView = (CardView)view.findViewById(R.id.cardview);
            mLineChart = (LineChart) view.findViewById(R.id.line_chart);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        http://stackoverflow.com/a/29255943/1635441
        int listViewItemType = getItemViewType(viewType);

        switch (listViewItemType) {
//          transactions - line chart
            case 0: return new LineChartViewHolder(LayoutInflater.from(context).inflate(R.layout.cardview_line_chart, parent, false));
//          auth vs settle
//            case 1: return new ViewHolder0(...);
////          successful vs failed
//            case 2: return new ViewHolder2(...);
//          % 3DS failures - pie chart
            case 3: return new PieChartViewHolder(LayoutInflater.from(context).inflate(R.layout.cardview_pie_chart, parent, false));
//          payment method breakdown - pie chart
            case 4: return new PieChartViewHolder(LayoutInflater.from(context).inflate(R.layout.cardview_pie_chart, parent, false));
        }

//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.movie_list_row, parent, false);
//
//        return new MyViewHolder(itemView);
        return null;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        int listViewItemType = holder.getItemViewType();

        switch (listViewItemType) {
//          transactions - line chart
            case 0:
//                return new LineChartViewHolder(LayoutInflater.from(getActivity()).inflate(R.layout.cardview_line_chart, parent, false));
//          auth vs settle
            case 1:
//                return new ViewHolder0(...);
//          successful vs failed
            case 2:
//                return new ViewHolder2(...);
//          % 3DS failures - pie chart
            case 3:
//                mPieChart.setData(generatePieData());
//                return new PieChartViewHolder(LayoutInflater.from(getActivity()).inflate(R.layout.cardview_pie_chart, parent, false));
//          payment method breakdown - pie chart
            case 4:
//                mPieChart.setData(generatePieData());
//                return new PieChartViewHolder(LayoutInflater.from(getActivity()).inflate(R.layout.cardview_pie_chart, parent, false));
        }

    }

    @Override
    public int getItemCount() {
        return 1; //moviesList.size();
    }
}
