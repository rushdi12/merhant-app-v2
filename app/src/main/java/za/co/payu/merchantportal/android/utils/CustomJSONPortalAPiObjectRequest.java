package za.co.payu.merchantportal.android.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Rushdi.Hoosain on 2016/01/30.
 */
public class CustomJSONPortalAPiObjectRequest extends JsonObjectRequest {

    SessionManager session;

    public CustomJSONPortalAPiObjectRequest(int method, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener, SessionManager session) {

        super(method, url, jsonRequest, listener, errorListener);
        this.session = session;


    }
    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {

        HashMap<String, String> user = session.getUserDetails();

        String token = user.get(SessionManager.KEY_TOKEN);
        String tokenType = user.get(SessionManager.KEY_TOKEN_TYPE);

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Authorization", tokenType + " " + token);

        return headers;
    }


    public RetryPolicy setRetryPolicy() {

        return new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 48,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

    }


    @Override
    public String getBodyContentType() {
        return "application/json";
    }


    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return super.getParams();
    }



}


