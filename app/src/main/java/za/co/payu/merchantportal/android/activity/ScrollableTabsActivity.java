package za.co.payu.merchantportal.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

import za.co.payu.merchantportal.android.R;
import za.co.payu.merchantportal.android.adapter.QrCodeAdapter;
import za.co.payu.merchantportal.android.fragments.ThisMonthFragment;
import za.co.payu.merchantportal.android.fragments.WeekFragment;
import za.co.payu.merchantportal.android.fragments.CustomRangeFragment;
import za.co.payu.merchantportal.android.fragments.YearToDateFragment;
import za.co.payu.merchantportal.android.fragments.LastMonthFragment;
import za.co.payu.merchantportal.android.fragments.YesterdayFragment;
import za.co.payu.merchantportal.android.fragments.TodayFragment;
import za.co.payu.merchantportal.android.utils.SessionManager;

public class ScrollableTabsActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private QrCodeAdapter dialogAdapter;

    // Session Manager Class
    SessionManager session;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrollable_tabs);

        // Session class instance
        session = new SessionManager(getApplicationContext());

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager.setCurrentItem(1);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_qrcode:
                //showQrCodeDialog();
                Intent i = new Intent(getApplicationContext(), QrCodeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
                startActivity(i);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new CustomRangeFragment(), "CUSTOMRANGE");
        adapter.addFrag(new TodayFragment(), "TODAY");
        adapter.addFrag(new YesterdayFragment(), "YESTERDAY");
        adapter.addFrag(new WeekFragment(), "WEEK");
        adapter.addFrag(new ThisMonthFragment(), "THISMONTH");
        adapter.addFrag(new LastMonthFragment(), "LASTMONTH");
        adapter.addFrag(new YearToDateFragment(), "YEARTODATE");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }



}
