package za.co.payu.merchantportal.android.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Rushdi.Hoosain on 2016/01/30.
 */


@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionResponse {

    @JsonProperty("links")
    private List<Link> links;

    @JsonProperty("content")
    private List<Content> content;

    @JsonProperty("page")
    private Page page;

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    public  List<Content> getContent() {
        return content;
    }

    public void setContent( List<Content> content) {
        this.content = content;
    }


    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }
}
