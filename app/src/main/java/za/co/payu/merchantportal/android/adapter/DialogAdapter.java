package za.co.payu.merchantportal.android.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import za.co.payu.merchantportal.android.R;
import za.co.payu.merchantportal.android.model.PaymentMethod;
import za.co.payu.merchantportal.android.model.TransactionSearchResponseDto;

public class DialogAdapter extends BaseAdapter {
    
    private Activity activity;
    private TransactionSearchResponseDto data;
    private static LayoutInflater inflater=null;
//    public ImageLoader imageLoader;
    
    public DialogAdapter(Activity a, TransactionSearchResponseDto d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        imageLoader=new ImageLoader(activity.getApplicationContext());
    }

    public int getCount() {
        return 1;
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }
    
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.fragment_transaction_detail, null);

        TextView merchantName = (TextView)vi.findViewById(R.id.merchantName); // title
        TextView transactionDate = (TextView)vi.findViewById(R.id.transactionDate); // artist name
        TextView transactionTime = (TextView)vi.findViewById(R.id.transactionTime);
        TextView transactionStatus = (TextView)vi.findViewById(R.id.transactionStatus);
        TextView basketAmount = (TextView)vi.findViewById(R.id.basketAmount); // duration
        TextView paymentMethod = (TextView)vi.findViewById(R.id.paymentMethodDetails);
//        ImageView thumb_image=(ImageView)vi.findViewById(R.id.list_image); // thumb image


        // Setting all values in listview
        merchantName.setText(data.getStoreName());
        transactionDate.setText(za.co.payu.merchantportal.android.utils.DateUtils.formatStringDateToPretty(data.getTransactionDate()));
//        transactionTime.setText(data.getTransactionDate());
        transactionStatus.setText(data.getTranactionStatus());
        basketAmount.setText(data.getBasketAmount());

        String paymentMethods = "" ;
//        String list = "";
        for(PaymentMethod pms :data.getPaymentMethodsUsed()){
            paymentMethods += pms.getPaymentMethodType();

        }
        paymentMethod.setText(paymentMethods);
//        imageLoader.DisplayImage(song.get(StreamManager.KEY_THUMB_URL), thumb_image);
        return vi;
    }
}