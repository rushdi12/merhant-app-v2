package za.co.payu.merchantportal.android.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import za.co.payu.merchantportal.android.R;
import za.co.payu.merchantportal.android.app.AppController;
import za.co.payu.merchantportal.android.fragments.QrCodeFragment;
import za.co.payu.merchantportal.android.utils.AlertDialogManager;
import za.co.payu.merchantportal.android.utils.ConnectionDetector;
import za.co.payu.merchantportal.android.utils.Constants;
import za.co.payu.merchantportal.android.utils.CustomJSONObjectRequest;
import za.co.payu.merchantportal.android.utils.NukeSSLCerts;
import za.co.payu.merchantportal.android.utils.SessionManager;

/**
 * A login screen that offers login via email/password.
 */
public class QrCodeActivity extends AppCompatActivity {
    public static final String TAG = QrCodeActivity.class
            .getSimpleName();

    private Toolbar toolbar;
    private RequestQueue mQueue;
    private ProgressDialog progressDialog;

    // Session Manager Class
    private SessionManager session;

    // flag for Internet connection status
    private boolean isInternetPresent = false;

    // Connection detector class
    private ConnectionDetector cd;
    private AlertDialogManager adm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_code);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        // creating connection detector class instance
        cd = new ConnectionDetector(getApplicationContext());

        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        adm = new AlertDialogManager();

        // check for Internet status
        if (!isInternetPresent) {
            adm.showAlertDialog(QrCodeActivity.this, "No Internet Connection",
                    "You don't have internet connection.", false);

        }

        // Session Manager
        session = new SessionManager(getApplicationContext());

        //*******************************************************************************
        // MUST BE REMOVED WHEN TAKING THIS TO PRODUCTION
        //*******************************************************************************
        NukeSSLCerts.nuke();
        //*******************************************************************************

        android.support.v4.app.FragmentManager fagManager = getSupportFragmentManager();

        if(savedInstanceState == null) {
            Fragment qrCodeFragment = new QrCodeFragment();

            fagManager.beginTransaction().replace(R.id.qrcode_fragment_container, qrCodeFragment).commit();
        }
    }


    @Override
    protected void onStop() {
        super.onStop();

        if(mQueue != null) {
            mQueue.cancelAll(TAG);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (item.getItemId()) {
            case android.R.id.home:
            case android.R.id.closeButton:
                onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}

