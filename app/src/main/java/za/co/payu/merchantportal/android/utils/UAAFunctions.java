package za.co.payu.merchantportal.android.utils;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.util.Pair;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import za.co.payu.merchantportal.android.activity.ScrollableTabsActivity;

/**
 * Created by Ramiz.Mohamed on 03/02/2016.
 */
public class UAAFunctions {
    private VolleyParser volleyParser;

    public UAAFunctions() {
        volleyParser =  new VolleyParser();
    }

    public JSONObject acquireToken(String username, String password, ProgressDialog pDialog){
        String url = Constants.environment_qa_uaa_address + Constants.environment_qa_uaa_context + Constants.uri_auth_token;

        // Building Parameters
        List<Pair> params = new ArrayList<Pair>();
        params.add(new Pair("grant_type", Constants.auth_uaa_grant_type));
        params.add(new Pair("client_id", Constants.auth_uaa_client_id));
        params.add(new Pair("redirect_uri", Constants.auth_uaa_redirect_uri));
        params.add(new Pair("username", username));
        params.add(new Pair("password", password));

        JSONObject response = volleyParser.getJSONFromUrl(url,pDialog,params);

//        try {
//
//            SharedPreferences settings = getSharedPreferences("UserInfo", 0);
//            SharedPreferences.Editor editor = settings.edit();
//            editor.putString("access_token",response.getString("access_token"));
//            editor.putString("token_type", response.getString("token_type"));
//            editor.commit();
//
//        } catch (JSONException e) {
//            Log.e("JSON parsing", e.getMessage());
//        }


        return response;
    }

    public boolean validateToken(){
        boolean valid = false;
        return valid;
    }

}
