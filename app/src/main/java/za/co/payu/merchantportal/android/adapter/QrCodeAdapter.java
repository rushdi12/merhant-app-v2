package za.co.payu.merchantportal.android.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import za.co.payu.merchantportal.android.R;
import za.co.payu.merchantportal.android.app.AppController;
import za.co.payu.merchantportal.android.model.TransactionSearchResponseDto;
import za.co.payu.merchantportal.android.utils.Constants;
import za.co.payu.merchantportal.android.utils.CustomJSONPortalAPiObjectRequest;
import za.co.payu.merchantportal.android.utils.LruBitmapCache;
import za.co.payu.merchantportal.android.utils.SessionManager;

public class QrCodeAdapter extends BaseAdapter {

    private Activity activity;
    private TransactionSearchResponseDto data;
    private static LayoutInflater inflater = null;

    private Button transactionStatusBtn;

    public static final String TAG = "Qrcode";

    // Progress Dialog
    private ProgressDialog pDialog;

    // Session Manager Class
    SessionManager session;

    ImageLoader mImageLoader;
    String url;

    View vi;

    public QrCodeAdapter(Activity a, String qrcodeUrl) {
        activity = a;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        imageLoader=new ImageLoader(activity.getApplicationContext());

        url = qrcodeUrl;


    }

    public int getCount() {
        return 1;
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.qrcode_layout_image, null);

        NetworkImageView qrCodeImageView = (NetworkImageView) vi.findViewById(R.id.qrcodeImageView);

        mImageLoader =  AppController.getInstance().getImageLoader();

        mImageLoader.get(url, ImageLoader.getImageListener(qrCodeImageView,
                0, 0));
        qrCodeImageView.setImageUrl(url, mImageLoader);


        transactionStatusBtn = (Button) vi.findViewById(R.id.transactionStatus);

        transactionStatusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity, "Get Status call", Toast.LENGTH_LONG).show();
            }
        });

        return vi;
    }


}