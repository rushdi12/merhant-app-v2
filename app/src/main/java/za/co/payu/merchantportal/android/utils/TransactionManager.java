package za.co.payu.merchantportal.android.utils;


import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class TransactionManager {
	private ArrayList<HashMap<String, String>> establishmentList = new ArrayList<HashMap<String, String>>();
	final static String TAG = TransactionManager.class.getSimpleName();
	
	// All static variables
	//static final String URL_SEARCH_BY_NAME_TYPE = "http://api.androidhive.info/music/music.xml";
//	final static String BASE_URL = new String("http://api.soundcloud.com");
//	final static String TRACKS_BASE_URL = new String("/tracks");
//	final static String USERS_BASE_URL = new String("/users/");
//	final static String USER_ID = new String("13922200");
//	final String QUERY_URL = new String("?q=");

//	static final String URL_SEARCH_BY_NAME_TYPE = BASE_URL + USERS_BASE_URL + USER_ID + TRACKS_BASE_URL + CLIENT_ID_URL + CLIENT_ID;
//192.168.115.1
    public final static String URL_GET_TRANSACTIONS = "http://10.0.3.2/h_appening/get_all_restaurants.php";

    public final static String URL_GET_TRANSACTION_BY_PAYUREFERENCE = "http://10.0.3.2/h_appening/get_all_restaurants.php";
    public final static String URL_GET_TRANSACTION_BY_MERCHANTREFERENCE = "http://10.0.3.2/h_appening/get_all_restaurants.php";

    public final static String URL_GET_TRANSACTIONS_BY_STORE = "http://10.0.3.2/h_appening/get_all_restaurants.php";

    public final static String URL_GET_TRANSACTIONS_FOR_TODAY = "http://10.0.3.2/h_appening/get_all_restaurants.php";
    public final static String URL_GET_TRANSACTIONS_FOR_TODAY_FOR_STORE = "http://10.0.3.2/h_appening/get_all_restaurants.php";

    public final static String URL_GET_TRANSACTIONS_FOR_WEEK = "http://10.0.3.2/h_appening/get_all_restaurants.php";
    public final static String URL_GET_TRANSACTIONS_FOR_WEEK_FOR_STORE = "http://10.0.3.2/h_appening/get_all_restaurants.php";

    public final static String URL_GET_TRANSACTIONS_FOR_LAST_WEEK = "http://10.0.3.2/h_appening/get_all_restaurants.php";
    public final static String URL_GET_TRANSACTIONS_FOR_LAST_WEEK_FOR_STORE = "http://10.0.3.2/h_appening/get_all_restaurants.php";

    public final static String URL_GET_TRANSACTION_DETAIL = "http://10.0.3.2/h_appening/get_all_restaurants.php";

	// JSON Node names
	public static final String KEY_ESTABLISHMENT = "restaurant";
    public static final String KEY_ID = "id";
	public static final String KEY_NAME = "name";
	public static final String KEY_TYPE = "type";
	public static final String KEY_LAT = "lat";
	public static final String KEY_LNG = "lng";
    public static final String KEY_FACEBOOK_URL = "facebookUrl";
    public static final String KEY_FACEBOOK_ID = "facebookId";
    public static final String KEY_DISTANCE = "distance";
	public static final String KEY_SUCCESS = "success";

    // JSON Node names
    public static final String KEY_BIO = "bio";
    public static final String KEY_ADDRESS1 = "addressLine1";
    public static final String KEY_ADDRESS2 = "addressLine2";
    public static final String KEY_ADDRESS3 = "addressLine3";
    public static final String KEY_NUM = "contactNumber";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_URL = "url";

    private String[] establishmentID;

	// Constructor
	public TransactionManager(){
		
	}
	
	/**
	 * Function to read all establishments from data
	 * and store the details in ArrayList
	 * */
	public ArrayList<HashMap<String, String>> getEstablishmentList(JSONObject list){

		Log.i(TAG, "Retrieving list of establishments");

        try {
            JSONArray establishments = null;

            // search for available establishments

            // Checking for SUCCES TAG
            int success = list.getInt(TransactionManager.KEY_SUCCESS);
            if (success == 1) {
                // restaurant found
                // getting Array of restaurant
                establishments = list.getJSONArray(KEY_ESTABLISHMENT);
//                displayTechData(restaurantData.toString());

                establishmentID = new String[establishments.length()];
                // looping through all technical data
                for (int i = 0; i < establishments.length(); i++) {
                    JSONObject establishment = establishments.getJSONObject(i);

                    // Storing each json item in variable
                    String id = establishment.getString(KEY_ID);
                    establishmentID[i] = id;

                    String estName = establishment.getString(KEY_NAME);
                    String estType = establishment.getString(KEY_TYPE);
                    String estLat = establishment.getString(KEY_LAT);
                    String estLng = establishment.getString(KEY_LNG);
                    String estFacebookUrl = establishment.getString(KEY_FACEBOOK_URL);
                    String identifier = estFacebookUrl.substring(estFacebookUrl.lastIndexOf("/"));
//                    http://stackoverflow.com/a/4576372/1635441
                    identifier = identifier.replace("/","");
                    // Creating new HashMap
                    HashMap<String, String> mapEstablishment = new HashMap<String, String>();

                    // adding each child node to HashMap key => value
                    mapEstablishment.put(KEY_ID, id);
                    mapEstablishment.put(KEY_NAME, estName);
                    mapEstablishment.put(KEY_TYPE, estType);
                    mapEstablishment.put(KEY_LAT, estLat);
                    mapEstablishment.put(KEY_LNG, estLng);
                    mapEstablishment.put(KEY_FACEBOOK_URL, estFacebookUrl);
                    mapEstablishment.put(KEY_FACEBOOK_ID, identifier);

                    // adding HashMap to ArrayList
                    establishmentList.add(mapEstablishment);
                }
            } else {
                // no technical data found
                Log.v("ERROR", "No data available for display");
                // display dialog and back to dashboard
                // Log.v("ERROR", "1");
//                error("There is no Restaurants data for this model!");
            }
        } catch (JSONException e) {
//            error("There has been an error please try again!");
            e.printStackTrace();
        }
				
		// return establishment list array
		return establishmentList;
	}
}
