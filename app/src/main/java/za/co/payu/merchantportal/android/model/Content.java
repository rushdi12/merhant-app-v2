package za.co.payu.merchantportal.android.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Rushdi.Hoosain on 2016/01/31.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Content {

    @JsonProperty("merchant_reference")
    private String merchantReference;

    @JsonProperty("payu_reference")
    private String payUReference;

    @JsonProperty("gateway_reference")
    private String gatewayReference;

    @JsonProperty("store_name")
    private String storeName;

    @JsonProperty("store_id")
    private String storeId;

    @JsonProperty("payment_type")
    private String paymentType;

    @JsonProperty("transaction_status")
    private String transactionStatus;

    @JsonProperty("transaction_type")
    private String transactionType;

    @JsonProperty("basket_amount")
    private String basketAmount;

   /* @JsonProperty("customer")
    private Customer customer;*/

    // Extras not in the BRS
    @JsonProperty("merchant_name")
    private String merchantName;

    @JsonProperty("received_time")
    private String receivedTime;

    @JsonProperty("transaction_amount")
    private String transactionAmount;

    @JsonProperty("fraud_status")
    private String fraudStatus;

    @JsonProperty("secure_3d_attempt")
    private String secure3DAttempt;

    @JsonProperty("response_time")
    private String responseTime;

    @JsonProperty("payment_methods")
    private List<PaymentMethod> paymentMethods;

    @JsonProperty("motherload_payu_reference")
    private String motherLoadPayUReference;

    @JsonProperty("expiry_date")
    private String expiryDate;

    @JsonProperty("last_updated")
    private String lastUpdated;

    @JsonProperty("current_payload_ref")
    private String currentPayloadRef;

    @JsonProperty("finalized_payload_ref")
    private String finalizedPayloadRef;

    @JsonProperty("payload_custom_field_list")
    private String payloadCustomFieldList;

    @JsonProperty("payload_status_history_list")
    private String payloadStatusHistoryList;

    @JsonProperty("authentication_type")
    private String authenticationType;

    @JsonProperty("channel")
    private String channel;

    @JsonProperty("channel_instrument")
    private String channelInstrument;

    @JsonProperty("final_state")
    private Boolean finalState;

    @JsonProperty("id")
    private String id;

    @JsonProperty("user_description")
    private String userDescription;

    // Merchant payload type fields
    @JsonProperty("stan")
    private String stan;

    @JsonProperty("payload_basket_list")
    private String payloadBasketList;

    @JsonProperty("payload_customer")
    private String payloadCustomer;

    @JsonProperty("return_url")
    private String returnURL;

    @JsonProperty("cancel_url")
    private String cancelURL;

    @JsonProperty("notification_url")
    private String notificationURL;

    @JsonProperty("ask_budget")
    private String askBudget;

 /*   @JsonProperty("fraud")
    private Fraud fraud;*/
/*
    @JsonProperty("links")
    private Links links;*/

    public String getBasketAmount() {
        return basketAmount;
    }

    public void setBasketAmount(String basketAmount) {
        this.basketAmount = basketAmount;
    }

    public String getMerchantReference() {
        return merchantReference;
    }

    public void setMerchantReference(String merchantReference) {
        this.merchantReference = merchantReference;
    }

    public String getPayUReference() {
        return payUReference;
    }

    public void setPayUReference(String payUReference) {
        this.payUReference = payUReference;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getGatewayReference() {
        return gatewayReference;
    }

    public void setGatewayReference(String gatewayReference) {
        this.gatewayReference = gatewayReference;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getFraudStatus() {
        return fraudStatus;
    }

    public void setFraudStatus(String fraudStatus) {
        this.fraudStatus = fraudStatus;
    }

    public String getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(String responseTime) {
        this.responseTime = responseTime;
    }

    public String getSecure3DAttempt() {
        return secure3DAttempt;
    }

    public void setSecure3DAttempt(String secure3DAttempt) {
        this.secure3DAttempt = secure3DAttempt;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getReceivedTime() {
        return receivedTime;
    }

    public void setReceivedTime(String receivedTime) {
        this.receivedTime = receivedTime;
    }

    public String getMotherLoadPayUReference() {
        return motherLoadPayUReference;
    }

    public void setMotherLoadPayUReference(String motherLoadPayUReference) {
        this.motherLoadPayUReference = motherLoadPayUReference;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getCurrentPayloadRef() {
        return currentPayloadRef;
    }

    public void setCurrentPayloadRef(String currentPayloadRef) {
        this.currentPayloadRef = currentPayloadRef;
    }

    public String getPayloadCustomFieldList() {
        return payloadCustomFieldList;
    }

    public void setPayloadCustomFieldList(String payloadCustomFieldList) {
        this.payloadCustomFieldList = payloadCustomFieldList;
    }

    public String getFinalizedPayloadRef() {
        return finalizedPayloadRef;
    }

    public void setFinalizedPayloadRef(String finalizedPayloadRef) {
        this.finalizedPayloadRef = finalizedPayloadRef;
    }

    public String getAuthenticationType() {
        return authenticationType;
    }

    public void setAuthenticationType(String authenticationType) {
        this.authenticationType = authenticationType;
    }

    public String getPayloadStatusHistoryList() {
        return payloadStatusHistoryList;
    }

    public void setPayloadStatusHistoryList(String payloadStatusHistoryList) {
        this.payloadStatusHistoryList = payloadStatusHistoryList;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getChannelInstrument() {
        return channelInstrument;
    }

    public void setChannelInstrument(String channelInstrument) {
        this.channelInstrument = channelInstrument;
    }

    public Boolean getFinalState() {
        return finalState;
    }

    public void setFinalState(Boolean finalState) {
        this.finalState = finalState;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserDescription() {
        return userDescription;
    }

    public void setUserDescription(String userDescription) {
        this.userDescription = userDescription;
    }

    public String getStan() {
        return stan;
    }

    public void setStan(String stan) {
        this.stan = stan;
    }

    public String getPayloadBasketList() {
        return payloadBasketList;
    }

    public void setPayloadBasketList(String payloadBasketList) {
        this.payloadBasketList = payloadBasketList;
    }

    public String getPayloadCustomer() {
        return payloadCustomer;
    }

    public void setPayloadCustomer(String payloadCustomer) {
        this.payloadCustomer = payloadCustomer;
    }

    public String getReturnURL() {
        return returnURL;
    }

    public void setReturnURL(String returnURL) {
        this.returnURL = returnURL;
    }

    public String getCancelURL() {
        return cancelURL;
    }

    public void setCancelURL(String cancelURL) {
        this.cancelURL = cancelURL;
    }

    public String getAskBudget() {
        return askBudget;
    }

    public void setAskBudget(String askBudget) {
        this.askBudget = askBudget;
    }

    public String getNotificationURL() {
        return notificationURL;
    }

    public void setNotificationURL(String notificationURL) {
        this.notificationURL = notificationURL;
    }

    public List<PaymentMethod> getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(List<PaymentMethod> paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    /*    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }*/
}


