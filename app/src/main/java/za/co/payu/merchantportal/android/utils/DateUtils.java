package za.co.payu.merchantportal.android.utils;

import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Rushdi.Hoosain on 2016/02/15.
 */
public final class DateUtils {


    private static String FORMAT_DATE_SERVER = "yyyy-MM-dd'T'HH:mm:ssZ";
    private static String timestampFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    private static String noTimeFormat = "yyyy-MM-dd";
    private static String pretty_format = "EEE, MMM d, ''yy";

    private DateUtils() {


    }

    public static String formatDateToTimestamp(Date date){
        String formattedDate = null;

        SimpleDateFormat sdf = new SimpleDateFormat(timestampFormat);

        formattedDate = sdf.format(date);
        System.out.format("%30s %s\n", timestampFormat, formattedDate);
        // sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        //   System.out.format("%30s %s\n", format, sdf.format(new Date(0)));

        return formattedDate;
    }

    public static String formatDateToPretty(String date){
        String formattedDate = null;

        SimpleDateFormat sdf = new SimpleDateFormat(pretty_format);

        formattedDate = sdf.format(date);
        System.out.format("%30s %s\n", timestampFormat, formattedDate);
        // sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        //   System.out.format("%30s %s\n", format, sdf.format(new Date(0)));

        return formattedDate;
    }



    public static Date convertStringToDate(String date, String format) {
        Date convertedDate = null;

        try {

            if (format == null) {
                format = timestampFormat;
            }

            SimpleDateFormat sdf = new SimpleDateFormat(format);
            convertedDate = sdf.parse(date);

        }catch (Exception e){
            Log.e("error pasing date", e.toString());
        }

        return convertedDate;
    }



    public static DateTime parseServerDateToLocal(String raw_date) {
        return DateTime.parse(raw_date, DateTimeFormat.forPattern(FORMAT_DATE_SERVER));
    }

    public static String formatStringDateToPretty(String dateString) {

        Date date = null;
        String formattedDate ="";
        try {

            //  "d MMM yyyy HH:mm:ss" 	Wed, 4 Jul 2001 12:08:56 -0700

            date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(dateString);

            formattedDate = new SimpleDateFormat("d MMM yyyy HH:mm:ss").format(date);

        } catch (ParseException e) {
            Log.e("Date parse error", e.toString());


        }

        return formattedDate;
    }
}

